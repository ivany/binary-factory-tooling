#!/usr/bin/python3
import os
import sys
import json
import yaml
import argparse

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine which jobs need to be registered in Jenkins.')
parser.add_argument('--static-jobs', type=str, required=True, dest='staticJobs')
parser.add_argument('--custom-jobs', type=str, required=True, dest='customJobs')
arguments = parser.parse_args()

# List of systems we can upload to
possibleServers = {
    'nicoda': {
        'username': 'sitedeployer',
        'hostname': 'nicoda.kde.org',
        'sshkey'  : '\$HOME/WebsitePublishing/website-upload.key'
    },
    'cdn': {
        'username': 'contentdeployer',
        'hostname': 'tinami.kde.org',
        'sshkey'  : '\$HOME/WebsitePublishing/cdn-upload.key'
    },
    'edulis': {
        'username': 'sitedeployer',
        'hostname': 'edulis.kde.org',
        'sshkey'  : '\$HOME/WebsitePublishing/edulis-upload.key'
    },
}

# Grab the list of conventional, static jobs in order to commence processing it
with open(arguments.staticJobs, 'r') as dataFile:
    # Parse the YAML file
    jobsToCreate = yaml.load( dataFile )

# Our output will be a list of Dictionaries, containing several keys:
# 1) The name of the job
# 2) The repository (or Subversion path) to be checked out
# 3) The branch of the repository to be checked out (in the case of Subversion, this will be blank)
# 4) The schedule on which the repository 
jobsGathered = []

# Let's get started processing conventional jobs!
for jobPipelineTemplate in jobsToCreate.keys():
    # We now go over each website that uses this particular pipeline template
    for website in jobsToCreate[ jobPipelineTemplate ]:
        # Construct the basic empty template for the job that will publish this website
        jobEntry = {
            'name': '',
            'repositoryUrl': '',
            'branch': 'master',
            'cron': '',
            'type': jobPipelineTemplate,
            'deploypath': '',
            'deploylangs': 'en',
            'deployserver': possibleServers['nicoda'],
        }

        # For the job entry we will need a name
        # To create this we take the domain this website is published at, and swap all dots with dashes
        jobEntry['name'] = website['domain'].replace('.', '-')

        # Does this website use Git for its repository?
        if 'repository' in website:
            jobEntry['repositoryUrl'] = 'https://invent.kde.org/' + website['repository'] + '.git'

        # Otherwise could it be using SVN for its repository?
        if 'svnpath' in website:
            jobEntry['repositoryUrl'] = 'svn://svn.kde.org/home/kde/' + website['svnpath']

        # Have we got a specified server to deploy this to?
        if 'server' in website:
            jobEntry['deployserver'] = possibleServers.get( website['server'], possibleServers['nicoda'] )

        # Along with the path it should be deployed at on the server
        jobEntry['deploypath'] = '/srv/www/generated/{0}/'.format( website['domain'] )

        # Finally we can add it to the list
        jobsGathered.append( jobEntry )

# Now we have to process custom jobs
# This is essentially done to calculate the Subversion/Git repository URL for them
with open(arguments.customJobs, 'r') as dataFile:
    # Load the list of custom jobs in
    jobsToCreate = json.load(dataFile)

# Process the custom jobs...
for customJob in jobsToCreate:
    # Construct the standard empty template for this job which we'll then populate
        jobEntry = {
            'name': '',
            'repositoryUrl': '',
            'branch': '',
            'cron': '',
            'type': '',
            'deploypath': '',
            'deploylangs': 'en',
            'deployserver': possibleServers['nicoda'],
        }

        # Transfer across most of the details we can
        jobEntry['name']       = customJob['name']
        jobEntry['branch']     = customJob['branch']
        jobEntry['cron']       = customJob['cron']
        jobEntry['type']       = customJob['type']
        jobEntry['deploypath'] = customJob['deploypath']

        if 'deploylangs' in customJob:
            jobEntry['deploylangs'] = customJob['deploylangs']

        # Does this website use Git for its repository?
        if 'repository' in customJob:
            jobEntry['repositoryUrl'] = 'https://invent.kde.org/' + customJob['repository'] + '.git'

        # Otherwise could it be using SVN for its repository?
        if 'svnpath' in customJob:
            jobEntry['repositoryUrl'] = 'svn://svn.kde.org/home/kde/' + customJob['svnpath']

        # Have we got a specified server to deploy this to?
        if 'server' in customJob:
            jobEntry['deployserver'] = possibleServers.get( customJob['server'], possibleServers['nicoda'] )

        # Finally we can add it to the list
        jobsGathered.append( jobEntry )

# Now output the jobs we've gathered in JSON to disk
# This will subsequently be read in by a Jenkins DSL script and turned into Jenkins Jobs
filePath = os.path.join( os.getcwd(), 'gathered-jobs.json')
with open(filePath, 'w') as jobsFile:
    json.dump( jobsGathered, jobsFile, sort_keys=True, indent=2  )

# All done!
sys.exit(0)
