def websiteDomain = name.replace('Website_', '').replace('-', '.')
def epubName = name.replace('Website_', '').replace('docs-', '').replace('-org', 'Manual').capitalize()

// Request a node to be allocated to us
node( "StaticWeb" ) {
// We want Timestamps on everything
timestamps {
    // We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
    catchError {
        // First Thing: Checkout Sources
        stage('Checkout Sources') {
            // Make sure we have a clean slate to begin with
            deleteDir()
            // Code
            checkout changelog: true, poll: true, scm: [
                $class: 'GitSCM',
                branches: [[name: gitBranch]],
                userRemoteConfigs: [[url: repositoryUrl]]
            ]
        }

        // Let's build website now
        stage('Website build') {
            sh """
                mkdir \$WORKSPACE/_site/
                mkdir \$WORKSPACE/_logs/

                export LC_ALL=C.UTF-8
                sphinx-intl build | tee -a \$WORKSPACE/_logs/intl-run.log

                export SITE_HOST="https://${websiteDomain}/"

                LANGUAGES="${deploylangs}"
                for langToBuild in \$LANGUAGES; do
                    sphinx-build -M html \$WORKSPACE/ \$WORKSPACE/staging/\$langToBuild/ -D language="\$langToBuild" -w \$WORKSPACE/_logs/warnings-\$langToBuild.log | tee -a \$WORKSPACE/_logs/build-\$langToBuild.log
                    sphinx-build -M epub \$WORKSPACE/ \$WORKSPACE/staging/\$langToBuild/ -D language="\$langToBuild"

                    mv $WORKSPACE/staging/\$langToBuild/html/ $WORKSPACE/_site/\$langToBuild/
                    mkdir $WORKSPACE/_site/\$langToBuild/epub/
                    mv $WORKSPACE/staging/\$langToBuild/epub/${epubName}.epub $WORKSPACE/_site/\$langToBuild/epub/
                done
            """
        }

        // Deploy the website!
        stage('Publishing Website') {
            sh """
                rsync -Hrlpvc --delete -e "ssh -i ${deploykey}" _site/ sitedeployer@nicoda.kde.org:${deploypath}
            """

            archiveArtifacts artifacts: '_logs/*.log', onlyIfSuccessful: false
        }
    }
}
}
